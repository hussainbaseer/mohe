
 
//  navbar-mobile


// slider-homepage
const mySlider = document.querySelectorAll(".mySlider"),
  index = document.querySelectorAll(".index");
let counter = 1;
var timer = setInterval(autoslide, 6000);

slideFun(counter);

function autoslide() {
  counter += 1;
  slideFun(counter);
}
function resetTimer() {
  if (typeof timer !== "undefined") {
    clearInterval(timer);
  }
  timer = setInterval(autoslide, 6000);
}

function plusSlides(n) {
  counter += n;
  slideFun(counter);
  resetTimer();
}
function currentSlide(n) {
  counter = n;
  slideFun(counter);
  resetTimer();
}
function slideFun(n) {
  let i;
  for (i = 0; i < mySlider.length; i++) {
    mySlider[i].style.display = "none";
    mySlider[i].classList.add("hidden");
  }
  for (i = 0; i < index.length; i++) {
    index[i].classList.remove("action");
  }
  if (n > mySlider.length) {
    counter = 1;
  }
  if (n < 1) {
    counter = mySlider.length;
  }
  if (mySlider[counter - 1].style.removeProperty) {
    mySlider[counter - 1].style.removeProperty("display");
  } else {
    mySlider[counter - 1].style.removeAttribute("display");
  }
  mySlider[counter - 1].classList.remove("hidden");
  mySlider[counter - 1].classList.add("md:grid", "md:grid-cols-2", "md:gap-4");
 
}

